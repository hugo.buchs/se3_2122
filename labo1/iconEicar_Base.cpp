//gcc -Wall -g -o iconEicar iconEicar.C -lstdc++

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#define PACK( __Declaration__ ) __pragma( pack(push, 1) ) __Declaration__ __pragma( pack(pop))
#define EICAR_SIZE 68
#define XOR_VAL 0xDEADBEEF

PACK(
typedef struct
{
	uint16_t 	ImageFileType ; 	/* Always 4D42h (''BM'')		*/
	uint32_t 	FileSize ;	        /* Physical file size in bytes 		*/
	uint16_t	Reserved1 ;
	uint16_t	Reserved2 ;
	uint32_t	ImageDataOffset ;	/* Start of image data offset in byte 	*/
} BITMAP_HEADER1);

PACK(
typedef struct
{
	uint16_t 	HeaderSize ;		/* Size of this header 			*/
	uint16_t	Reserved1;
	uint16_t	ImageWidth ;		/* Image width in pixel			*/
	uint16_t	Reserved2;
	uint16_t	ImageHeight ;		/* Image height in pixel			*/
	uint16_t	Reserved3;
	uint16_t	NumberOfImagePlanes ;/* always 1				*/
	uint16_t	BitPerPixel ;		/* 1, 4, 8, 24				*/
	uint32_t	CompressionMethod ;	/* 0, 1, 2				*/
	uint32_t	SizeOfBitmap ;		/* Size of the bitmap in bytes		*/
	uint32_t	HorzResolution ;	/* Horiz. Resolution in pixel per meter	*/
	uint32_t	VertResolution ;	/* Vert. Resolution in pixel per meter	*/
	uint32_t	NumColorsUsed ;	    /* Number of the colors in the bitmap	*/
	uint32_t	NumSignificantColors ;	/* Number of important colors in palette	*/
} BITMAP_HEADER2);

PACK(typedef struct
{
	uint8_t 	Blue ;
	uint8_t		Green ;
	uint8_t		Red ;
}RGB);



static void addEicarVirus (const char *pSrc, const char *pDst, const char *pEicarVirus)
{

	FILE          *p1 = 0;
	FILE          *p2 = 0;
	FILE          *eicar = 0;
	BITMAP_HEADER1 h1;
    BITMAP_HEADER2 h2;

    /* Open the files */
    p1 = fopen(pSrc, "rb");
    p2 = fopen(pDst, "wb");
	eicar = fopen(pEicarVirus, "rb");

	if (p1 != 0 && p2 != 0)
	{
		// Read the two headers
		int pos = fread  (&h1, 1, sizeof(h1), p1);
		if(pos > 0 && h1.ImageFileType == 0x4D42){
			printf("Test\r\n");
			pos += fread  (&h2, 1, sizeof(h2), p1);
			printf("Position : %d\r\n", pos);
			printf("Offset : %d\r\n", h1.ImageDataOffset);
			printf("Size : %d\r\n", h2.SizeOfBitmap);

			fseek(p1, h1.ImageDataOffset, SEEK_SET);

			int nb_byte_line = (h2.ImageWidth * sizeof(RGB)) + (h2.ImageWidth % 4);
			RGB* p_buffer = (RGB*)malloc(nb_byte_line);
			if (p_buffer) {
				int padding_size = h1.ImageDataOffset - (sizeof(h1) + sizeof(h2));
				char* padding = (char*)calloc(padding_size, 1);
				if (padding) {

					fwrite(&h1, 1, sizeof(h1), p2);
					fwrite(&h2, 1, sizeof(h2), p2);

					fwrite(padding, 1, padding_size, p2);

					for (int i = 0; i < h2.ImageHeight; i++) {
						fread(p_buffer, 1, nb_byte_line, p1);
						for (int j = 0; j < h2.ImageWidth; j++) {
							RGB color = p_buffer[j];
							uint8_t gray = color.Blue/3 + color.Red/3 + color.Green/3;
							p_buffer[j].Blue = gray;
							p_buffer[j].Red = gray;
							p_buffer[j].Green = gray;
						}
						for (int j = 0; j < h2.ImageWidth/2; j++) {
							RGB color_temp = p_buffer[j];
							p_buffer[j] = p_buffer[h2.ImageWidth - 1 - j];
							p_buffer[h2.ImageWidth - 1 - j] = color_temp;
						}
						fwrite(p_buffer, 1, nb_byte_line, p2);
					}

					int eicarBuffer[EICAR_SIZE];

					fread(&eicarBuffer, sizeof(int), EICAR_SIZE/sizeof(int), eicar);

					for (int i = 0; i < EICAR_SIZE / sizeof(int); i++) {
						eicarBuffer[i] = eicarBuffer[i] ^ XOR_VAL;
					}

					fwrite(&eicarBuffer, sizeof(int), EICAR_SIZE / sizeof(int), p2);
				}
				free(padding);
			}
			free(p_buffer);
		}
		
		fclose(p1);
		fclose(p2);
	}
	else
	{
		printf("Error ! Could not open files.");
	} 
}

int main(void)
{
	addEicarVirus("icon.bmp", "icon2.bmp", "eicar.com");
	return 0;
}