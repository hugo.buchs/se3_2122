#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib, "Ws2_32.lib")
#endif

// gcc -Wall -o client_ipv4_ipv6 client_ipv4_ipv6.c

#include "common.h"
#include "sys/types.h"
#include "winsock2.h"
#include "ws2tcpip.h"
#include "string.h"
#include "stdio.h"
#include "stdlib.h" 

#define NB_LOOP     3

SOCKET s;

static int socketIPv4(void);

BOOL WINAPI consoleHandler(DWORD signal) {

    if (signal == CTRL_C_EVENT) {
        printf("Ctrl-C handled\n"); // do cleanup
        close(s);
        WSACleanup();
    }

    return TRUE;
}

int main()
{
    if (!SetConsoleCtrlHandler(consoleHandler, TRUE)) {
        printf("Error: Could not set control handler");
        return 1;
    }
    socketIPv4();
    return 0;
}

static int socketIPv4(void)
{
struct sockaddr_in  sin;
struct msg message;
int res;

    WSADATA wsaData;
    int iResult;

    // Initialize Winsock
    iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
    if (iResult != 0) {
        printf("WSAStartup failed: %d\n", iResult);
        return 1;
    }

    s = socket (AF_INET, SOCK_DGRAM, 0);
    memset (&sin, 0, sizeof(sin));
    sin.sin_family = AF_INET;
    inet_pton(AF_INET, "127.0.0.1", &sin.sin_addr);
    sin.sin_port = htons(1234);
    connect (s, (struct sockaddr *) &sin, sizeof(sin));

    printf("Start\n");
    
    for (int i=0; i<NB_LOOP; i++)
    {
        
        printf ("Enter value 1: \n");
        while (scanf("%hi", &message.val1) <= 0) {};
        message.val1 = htons(message.val1);
        printf("Enter operation: \n");
        while(scanf("%hu", &message.operation) <= 0) {};
        printf("Enter value 2: \n\n");
        while(scanf("%hi", &message.val2) <= 0) {};
        message.val2 = htons(message.val2);
        send(s, (const char*)&message, sizeof(message), 0);
        recv(s, (char *)&res, sizeof(int), 0);
        printf ("Answer: %d\n\n", ntohl(res));
    }
    closesocket(s);
    printf("stop\n");

    WSACleanup();

    return 0;
}