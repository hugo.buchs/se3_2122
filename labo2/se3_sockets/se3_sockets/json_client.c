#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib, "Ws2_32.lib")
#endif

#include "sys/types.h"
#include "winsock2.h"
#include "ws2tcpip.h"
#include "string.h"
#include "stdio.h"
#include "stdlib.h" 


static int socketIPv4(void);

int main()
{

    socketIPv4();
    return 0;
}


static int socketIPv4(void)
{
    char *request = "POST / HTTP/1.1\r\n" \
                 "Host: 127.0.0.1 : 8008\r\n" \
                 "Content-Length: 44\r\n" \
                 "Content-Type: application/json; charset = UTF-8\r\n" \
                 "User-agent: Python - httplib2/0.19.1 (gzip)\r\n" \
                 "Accept-Encoding: gzip, deflate\r\n\r\n" \
                 "{\"operation\": \"add\", \"val1\": 34, \"val2\": 56}";

    struct sockaddr_in  sin;
    int		    s;
    int res;
    char                buffer2[32];
    int                 i;

    WSADATA wsaData;
    int iResult;

    // Initialize Winsock
    iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
    if (iResult != 0) {
        printf("WSAStartup failed: %d\n", iResult);
        return 1;
    }

    s = socket(AF_INET, SOCK_STREAM, 0);
    memset(&sin, 0, sizeof(sin));
    sin.sin_family = AF_INET;
    //sin4.sin_addr.s_addr = inet_addr("127.0.0.1");
    inet_pton(AF_INET, "127.0.0.1", &sin.sin_addr);
    sin.sin_port = htons(8008);
    connect(s, (struct sockaddr*) & sin, sizeof(sin));

    printf("start\n");

    send(s, request, strlen(request), 0);

    char response_buffer[1024];
    
    recv(s, response_buffer, 1024, MSG_WAITALL);
    closesocket(s);
    char* json_start = response_buffer;
    while (!(json_start[0] == '{')) json_start++;
    char* response_tmp = json_start+1;
    while (!(response_tmp[0] == '}')) response_tmp++;
    response_tmp[1] = '\0';
    printf("JSON Response: %s\r\n", json_start);

    printf("stop\n");

    WSACleanup();

    return 0;
}