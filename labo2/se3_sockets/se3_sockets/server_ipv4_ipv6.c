#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib, "Ws2_32.lib")
#endif

// gcc -Wall -o server_ipv4_ipv6 server_ipv4_ipv6.c

#include "common.h"
#include "sys/types.h"
#include "winsock2.h"
#include "ws2tcpip.h"
#include "string.h"
#include "stdio.h"
#include "signal.h"

SOCKET s;

static int socketIPv4(void);

BOOL WINAPI consoleHandler(DWORD signal) {

    if (signal == CTRL_C_EVENT) {
        printf("Ctrl-C handled\n"); // do cleanup
        close(s);
        WSACleanup();
    }

    return TRUE;
}

int main()
{
    if (!SetConsoleCtrlHandler(consoleHandler, TRUE)) {
        printf("Error: Could not set control handler");
        return 1;
    }
    socketIPv4();

    return 0;
}

static int socketIPv4(void)
{
struct sockaddr_in  from;
struct sockaddr_in  sin;
int                 alen;
struct msg          message;
int                 res;

    WSADATA wsaData;
    int iResult;

    // Initialize Winsock
    iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
    if (iResult != 0) {
        printf("WSAStartup failed: %d\n", iResult);
        return 1;
    }
    s = socket (AF_INET, SOCK_DGRAM, 0);
    memset (&sin, 0, sizeof(sin));
    sin.sin_family      = AF_INET;
    sin.sin_addr.s_addr = INADDR_ANY;
    sin.sin_port        = htons(1234);

    alen = bind (s, (struct sockaddr*) &sin, sizeof (sin));

    printf("Server running...\n");
    for (;;)
    {
        alen = sizeof (from);
        memset (&message, 0, sizeof(message));
        recvfrom (s, (char*) &message, sizeof(message), 0, (struct sockaddr*) &from, &alen);
        short val1 = ntohs(message.val1);
        short val2 = ntohs(message.val2);
        if (message.operation == 1) {
            res = val1 + val2;
        }
        else {
            res = val1 - val2;
        }
        res = htonl(res);
        sendto (s, (char*) &res, sizeof(res), 0, (struct sockaddr*)&from, alen);
    }
    closesocket(s);
    WSACleanup();
    return 0;
}





